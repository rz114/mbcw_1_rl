#https://www.learnopencv.com/multitracker-multiple-object-tracking-using-opencv-c-python/
import numpy as np
from matplotlib import pyplot
import cv2
from matplotlib import cm
from matplotlib import colors
import matplotlib.pyplot as plt
import importlib
import time
importlib.import_module('mpl_toolkits.mplot3d').__path__


tracker_types = ['BOOSTING', 'MIL','KCF', 'TLD', 'MEDIANFLOW', 'GOTURN', 'MOSSE', 'CSRT']
tracker_type = tracker_types[2]
tracker = cv2.TrackerKCF_create()


def getContours(image):
    w, h, d = image.shape 
    if d != 1:
        blur = cv2.GaussianBlur(image,(5,5),1)
        rgb_img = cv2.cvtColor(blur, cv2.COLOR_BGR2RGB)
        hsv_img = cv2.cvtColor(rgb_img, cv2.COLOR_RGB2HSV)

        ## getting black HSV color representation for low and high color
        black_low = (0, 0, 0)
        black_high = (250, 185, 61)

        black_mask = cv2.inRange(hsv_img, black_low, black_high)
        cv2.imshow('Color Segmentation', black_mask)
        result = cv2.bitwise_and(image, image, mask=black_mask)

        ## converting the HSV image to to RGB then RGB to Gray inorder to be able to apply
        ## contouring
        RGB_again = cv2.cvtColor(hsv_img, cv2.COLOR_HSV2RGB)
        gray = cv2.cvtColor(RGB_again, cv2.COLOR_RGB2GRAY)

        edge = cv2.Canny(gray, 120, 150)
        cv2.imshow('edge', edge)
        cv2.imshow('MASK', result)
        #cv2.VideoWriter('CannyEdge.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (image.size,h))

        ret, threshold = cv2.threshold(gray, 40, 180,cv2.THRESH_BINARY_INV)

        contours, hierarchy = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        cnt = sorted(contours, key=cv2.contourArea) # sorting the contour ascending
        count = 0
        for i in contours:
            count = count +1

        #cv2.putText(image, "Contours count"  + str(count), (100,100), cv2.FONT_HERSHEY_SIMPLEX, 0.75,(0,0,255),2)

        
        if cnt != None:
            # Getting the x and y, width and height of the biggest contour
            x,y,w,h = cv2.boundingRect(cnt[-1]) 
            cv2.rectangle(image,(x,y),(x + w,y + h),(0,255,0),2)
            bbox1 = x,y,w,h
            # Getting the x and y, width and height of the 2nd biggest contour
            x1,y1,w1,h1 = cv2.boundingRect(cnt[-2]) 
            cv2.rectangle(image,(x1,y1),(x1 + w1,y1 + h1),(0,255,0),2)
            bbox2 = x1,y1,w1,h1
        

        cv2.imshow('Detection', image)
        return bbox1, bbox2
       



#"video frames capture and show the canny edge image of a detected object in frames."

cap = cv2.VideoCapture('data/Coursework/1.MP4')
# We give some time for the camera to warm-up!
    
# Exit if video not opened.
if not cap.isOpened():
    print("Could not open video")
    sys.exit()
 
# Read first frame.
ok, frame = cap.read()
if not ok:
   print('Cannot read video file')
   sys.exit()

#To Get and show the Canny Edge Detection of the video 

 
def display_video():
    # Read a new frame
    ok, frame = cap.read()
    w, h, d = frame.shape
    while True:
      ok, frame = cap.read()
      if ok:      
           # # Start timer
           timer = cv2.getTickCount()
           time.sleep(0.02)

           
           # Initialize MultiTracker
           multiTracker = cv2.MultiTracker_create()

           # Get the Boxes of the two detected object
           bbox = getContours(frame)
    

           for box in bbox:
             multiTracker.add(tracker, frame, box)

           # Update tracker
           # get updated location of objects in subsequent frames
           success, boxes = multiTracker.update(frame)
 
           # Calculate Frames per second (FPS) or set fps
           fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)
            #fps = 100
 
           # draw tracked objects
           for i, newbox in enumerate(boxes):
               p1 = (int(newbox[0]), int(newbox[1]))
               p2 = (int(newbox[0] + newbox[2]), int(newbox[1] + newbox[3]))
               cv2.rectangle(frame, p1, p2, (255,0,0), 2, 1)
           else :
                # Tracking failure
                cv2.putText(frame, "Tracking failure detected", (100,80), cv2.FONT_HERSHEY_SIMPLEX, 0.75,(0,0,255),2)
  

            # Display tracker type on frame
           cv2.putText(frame, tracker_type + " Tracker", (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50),2)
     
            # Display FPS on frame
           cv2.putText(frame, "FPS : " + str(int(fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)
 
           # Display result
           cv2.imshow('MultiTracker', frame)
           #cv2.VideoWriter('MultiTracker.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (frame.size,h))
           cv2.waitKey(1)

           if cv2.waitKey(20) == ord('q'):  # Introduce 20 milisecond delay.  press q to exit.
               break



display_video()

cv2.destroyAllWindows()