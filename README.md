# README #

Computer Vision CW Mobile Robotics
### What is this repository for? ###

Edge Detection and Color Segmentation to form a mask for CW MB


The objective of this assignment is to be able to detect obstacles in amoving land rover scenario. 
In this task, you will be employing acombination/series of image processing techniques to isolate the defined obstacles from the scene

### Libraries used Used ###

OpenCV in Python for Canny Edge, Colour Segmentation, Background Subtraction and KFC Tracking

### Contribution guidelines ###

RZ
LT

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact